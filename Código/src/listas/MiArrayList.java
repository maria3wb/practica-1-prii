/*
 * Copyright 2018 Javier Zudaire, Maria Paredes y Elisa Coello
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package listas;

/**
 * ArrayList genérico
 *
 * @param <E>
 */
public class MiArrayList<E extends Comparable> {

    int tamMax = 100;
    int tam = 0;
    private Object[] array = new Object[tamMax];

    /**
     * Añade elementos a el ArrayList
     *
     * @param e
     */
    public void add(E e) {
        if (tam == tamMax) {
            duplicate();
        }
        array[tam++] = e;
    }

    /**
     * Obtiene el elemento de un índice específico
     *
     * @param i
     * @return
     */
    public E get(int i) {
        if (i >= 0 && i < array.length) {
            return (E) array[i];
        } else {
            return null;
        }
    }

    private void duplicate() {
        tamMax = tamMax * 2;

        Object[] nuevoArray = new Object[tamMax];

        for (int i = 0; i < array.length; i++) {
            nuevoArray[i] = array[i];
        }

        array = nuevoArray;
    }

    /**
     * Devuelve true si el ArrayList no contiene ningún elemento
     *
     * @return
     */
    public boolean isEmpty() {

        return array[0] == null;

    }

    /**
     * Elimina un elemento específico
     *
     * @param e
     */
    public void remove(E e) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] == e) {
                desplazar(i);
                break;
            }
        }
    }

    private void desplazar(int fromIndex) {
        for (int i = fromIndex; i < array.length - 1; i++) {
            array[i] = array[i + 1];
        }
    }

    /**
     * Devuelve true si el ArrayList contiene un elemento específico
     *
     * @param e
     * @return
     */
    public boolean contains(E e) {
        if (array[0] == null) {
            return false;
        }

        for (Object element : array) {
            if (element.equals(e)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determina el tamaño del ArrayList
     *
     * @return
     */
    public int size() {
        int size = 0;
        if (array[0] == null) {
            return 0;
        } else {
            int count = 0;
            for (Object obj : array) {
                if (obj != null) {
                    count++;
                }
            }

            return count;
        }

    }

}
